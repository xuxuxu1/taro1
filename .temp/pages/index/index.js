import Nerv from "nervjs";
import Taro from "@tarojs/taro-h5";
import { Text } from '@tarojs/components';
import { AtList, AtListItem } from 'taro-ui';
import './index.scss';
export default class Index extends Taro.Component {
  constructor() {
    super(...arguments);
    /**
     * 指定config的类型声明为: Taro.Config
     *
     * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
     * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
     * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
     */
  }
  componentWillMount() {}
  componentDidMount() {}
  componentWillUnmount() {}
  componentDidShow() {}
  componentDidHide() {}
  render() {
    return <AtList>
      
      
      
      <AtListItem title="请选择服务" hasBorder={false} className={'list-font-size'}>
        <slot><Text>请选择服务</Text></slot>
      </AtListItem>
      <AtListItem title="标题文字" />
      <AtListItem title="标题文字" arrow="right" />
      <AtListItem title="标题文字" extraText="详细信息" note="描述信息" />
      <AtListItem title="禁用状态" disabled extraText="详细信息" />
    </AtList>;
  }
  config = {
    navigationBarTitleText: '学杂费缴费'
  };
}