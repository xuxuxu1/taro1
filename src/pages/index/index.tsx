import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text } from '@tarojs/components'
import {AtAvatar, AtList, AtListItem} from 'taro-ui'
import './index.scss'

export default class Index extends Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '学杂费缴费'
  }

  render () {
    return (
    <AtList>
      {/*<View className='index'>*/}
      {/*  <Text>请选择服务</Text>*/}
      {/*</View>*/}
      <AtListItem title='请选择服务'  hasBorder={false} className={'list-font-size'}>
        <slot><Text>请选择服务</Text></slot>
      </AtListItem>
      <AtListItem title='标题文字'  />
      <AtListItem title='标题文字' arrow='right' />
      <AtListItem title='标题文字' extraText='详细信息' note='描述信息'/>
      <AtListItem title='禁用状态' disabled extraText='详细信息' />
    </AtList>
    )
  }
}
